/* implementation of the FastMap dimensionality reduction algorithm
 * due to Faloutsos and Lin
 * $Id: fastmap.c 4 2007-07-21 19:23:13Z nguyenda $
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include <math.h>
#include <unistd.h>
#include <float.h>

#define ROW(r,dim) ((dim)*(r) - ((r)*((r)+1)/2))
#define GET(x,data,dim,i,j)			\
  do {\
    if ((i) == (j)) (x) = 0.f; \
    else if ((i) > (j)) (x) = (data)[ROW(j,dim) + ((i)-(j)-1)];	\
    else (x) = (data)[ROW(i,dim) + ((j)-(i)-1)];			\
  } while (0)

#define SET(data,dim,i,j,x)			\
  do {\
    if ((i) > (j)) (data)[ROW(j,dim) + ((i)-(j)-1)] = x;	\
    else if ((i) < (j)) (data)[ROW(i,dim) + ((j)-(i)-1)] = x;	\
  } while (0)



static void
error (const char *mesg, ...)
{
  va_list ap;
  va_start (ap, mesg);
  fputs ("error: ** ", stderr);
  vfprintf (stderr, mesg, ap);
  fputc ('\n', stderr);
  va_end (ap);
  exit (1);
}

inline static double 
valof (const double *matrix, int dim, int r, int c)
{
  double x;
  GET (x, matrix, dim, r, c);
  return x;
}

/*
 * read in a upper symmetric square distance matrix (without 
 * diagonal values) for example, consider the following 3x3 
 * distance matrix:

[0,0] [0,1] [0,2]
[1,0] [1,1] [1,2]
[2,0] [2,1] [2,2]

  * this matrix is represented as follows:

3
[0,1] [0,2]
[1,2]

  * where the diagonal elements are 0s
  */
double *
fm_read_distance_matrix (int *dim, FILE *fp)
{
  char token[BUFSIZ] = {0}; /* current token */
  int toklen = 0, cell = 0, line = 0;
  double *matrix = 0;

  /* 
   * the first value in the stream is the dimension of the matrix
   */
  fgets (token, sizeof (token), fp);
  *dim = atoi (token);
  if (*dim <= 0)
    error ("bogus matrix dimension: %d", *dim);

  matrix = malloc (sizeof (*matrix) * (*dim*(*dim-1)/2));
  assert (matrix != 0);

  while (!feof (fp))
    {
      int ch = fgetc (fp);
      switch (ch)
	{
	case '\n':
	case ',': 
	  if (toklen == 0)
	    error ("missing value in line %d", line+1);

	  token[toklen] = 0;
	  toklen = 0;

	  matrix[cell] = strtod (token, 0);
	  if (matrix[cell] < 0.)
	    error ("negative distance (%f) in line %d", 
		   matrix[cell], line+1);
	  ++cell;

	  if (ch == '\n')
	    ++line;
	  break;

	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
	case '.':
	case 'e':
	case 'E':
	case '-':
	  token[toklen++] = ch;
	  break;

	default:
	  if (ch > 0)
	    error ("unknown character '%c' at line %d", ch, line+1);
	}
    }

  return matrix;
}

/*
 * compute euclidean distance for row vectors in V and return
 * the upper triangular matrix of the distance matrix
 */
static double
euclidean (const double *x, const double *y, int len)
{
  int i;
  double d, s = 0.;

  for (i = 0; i < len; ++i)
    {
      d = x[i] - y[i];
      s += d*d;
    }
  return sqrt (s);
}

double *
fm_euclidean (int *dim, const double *V, int row, int col)
{
  double *euc = malloc (row * (row-1)/2 * sizeof (double));
  int i, j, c = 0;

  assert (euc != 0);

  for (i = 0; i < row; ++i)
    for (j = i+1; j < row; ++j)
      euc[c++] = euclidean (V + i*col, V + j*col, col);
  
  assert (c == row*(row-1)/2);
  *dim = row;

  return euc;
}

/*
 * read in row vectors in csv format; the first line in the stream
 * must be the dimension of the matrix, e.g.,
 * 2 3
 * 1,2,3
 * 2,1,3
 */
double *
fm_read_data_matrix (int *dim, FILE *fp)
{
  double *matrix, *M;
  char token[BUFSIZ] = {0};
  int toklen = 0, count = 0, c = 0, line = 0;
  int row, col;

  fgets (token, sizeof (token), fp);
  if (sscanf (token, "%d %d", &row, &col) != 2)
    error ("unable to parse matrix dimension");
  
  if (row == 0 || col == 0)
    error ("bogus matrix dimension: %dx%d\n", row, col);

  matrix = malloc (row * col * sizeof (*matrix));
  assert (matrix != 0);

  while (!feof (fp))
    {
      int ch = fgetc (fp);
      switch (ch)
	{
	case '\n':
	case ',': 
	  if (toklen == 0)
	    error ("missing value in line %d, column %d", line+1, c+1);

	  token[toklen] = 0;
	  toklen = 0;

	  matrix[count++] = strtod (token, 0);

	  ++c;
	  if (ch == '\n')
	    {
	      ++line;
	      if (c != col)
		error ("row %d has %d values while %d are expected", 
		       line, c, col);
	      c = 0;
	    }
	  break;

	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
	case '.':
	case 'e':
	case 'E':
	case '-':
	  token[toklen++] = ch;
	  break;

	default:
	  if (ch > 0)
	    error ("unknown character '%c' at line %d", ch, line+1);
	}
    }

  /*
   * now compute the euclidean distance matrix
   */
  M = fm_euclidean (dim, matrix, row, col);
  free (matrix);

  return M;
}


double *
fm_read_distance_matrix_file (int *dim, const char *file)
{
  double *m = 0;
  FILE *fp = fopen (file, "r");
  if (fp != 0)
    {
      m = fm_read_distance_matrix (dim, fp);
      fclose (fp);
    }
  else
    error ("can't open matrix file: %s", file);

  return m;
}

static double
project_distance (int a, int b,  int h, int k,
		  const double *X, const double *M, int dim)
{
  int i;
  double d = valof (M, dim, a, b), ab;

  for (i = 0; i < h; ++i)
    {
      ab = X[a*k+i] - X[b*k+i];
      /* it's conceivable that the insde of sqrt() is negative 
	 (e.g., the original distance metric isn't euclidean) */
      d = sqrt (fabs (d*d - ab*ab));
    }
  return d;
}

static double
find_maximal_pivots (int *a, int *b, int iter, int h, int k,
		     const double *X, const double *M, int dim)
{
  int i, j;
  double max = -DBL_MAX;

  *a = 0;
  *b = (int)(dim * (double)(rand () / (RAND_MAX + 1.)));
  for (i = 0; i < iter; ++i)
    {
      for (j = 0; j < dim; ++j)
	if (*b != j)
	  {
	    double x = project_distance (*b, j, h, k, X, M, dim);
	    if (x > max)
	      {
		max = x;
		*a = j;
	      }
	  }
      for (j = 0; j < dim; ++j)
	if (*a != j)
	  {
	    double x = project_distance (*a, j, h, k, X, M, dim);
	    if (x > max)
	      {
		max = x;
		*b = j;
	      }
	  }
    }

  return max;
}

void
fm_fastmap (double *X, int *P, int k, const double *M, int dim)
{
  /*
   * X -- input/output dim x k matrix of embedded coordinates
   * P -- input/output 2 x k matrix of pivots
   * k -- input embeded dimension
   * M -- input distance matrix
   * dim - dimension of distance matrix
   */
  int j, a, b;
  double ab;

  for (j = 0; j < k; ++j)
    {
      /* step 2: choose pivots */
      find_maximal_pivots (&a, &b, 5, j, k, X, M, dim);

      /* step 3: save pivots */
      P[j] = a; P[k+j] = b; /* save pivots */

      /* step 5: */
      ab = project_distance (a, b, j, k, X, M, dim);
      if (ab > 0.f)
	{
	  int i;
	  double ab2 = 2.*ab, ab_ab = ab*ab, ai, bi;
	  
	  for (i = 0; i < dim; ++i)
	    {
	      /* project this object onto a-b */

	      ai = project_distance (i, a, j, k, X, M, dim);
	      bi = project_distance (i, b, j, k, X, M, dim);

	      X[i*k+j] = (ai*ai + ab_ab - bi*bi)/ab2;
	    }
	}
      /* step 4: */
      else
	{
	  int i;
	  for (i = 0; i < dim; ++i)
	    X[i*k+j] = 0.f;
	}
    }
}

static void
usage (const char *prog)
{
  fprintf (stderr, "\
usage: %s [option]\n\
Dimensionality reduction using the FastMap algorithm where [option]\n\
is one or more of the following:\n\
-k D  the dimension of the embedded subspace (default: 2)\n\
\n\
-M    input format is row-data matrix of the form:\n\
        3 3\n\
        1,2,3\n\
        2,1,3\n\
        2,3,1\n\
      where the first line specifies the dimension (row x col) of the\n\
      matrix.  If not specified, the default input format is assumed to\n\
      be the distance matrix (see below).\n\
\n\
-S    input is upper triangular of the similarity (as opposed to\n\
      distance) matrix\n\
\n\
-h    print this message\n\
\n\
The default (i.e., if -M is not specified) input format is the \n\
upper triangular of the distance matrix based on an appropriate\n\
metric space (e.g., Euclidean).  Consider, for example, the folling\n\
distance matrix of size 5x5:\n\
\n\
  0   1   1 100 100\n\
  1   0   1 100 100\n\
  1   1   0 100 100\n\
100 100 100   0   1\n\
100 100 100   1   0\n\
\n\
In this case, the following format is used as input\n\
\n\
5\n\
1,1,100,100\n\
1,100,100\n\
100,100\n\
1\n\
\n\
which is simply the upper triangular of the distance matrix.\n\
The first line is the dimension (5 above) of the square matrix.\n\
\n"
	   , prog);
  exit (1);
}

int
main (int argc, char *argv[])
{
  double *M;
  int dim, k = 2, opt;
  int data = 0; /* is data matrix? */
  int similarity = 0; /* is similarity or distance matrix? */
  FILE *output = stdout;
  const char *prog = strrchr (argv[0], '/');

  if (prog != 0) ++prog;
  else prog = argv[0];

  while ((opt = getopt (argc, argv, "k:MSh")) != EOF)
    {
      switch (opt)
	{
	case 'k': k = atoi (optarg); break;
	case 'M': data = 1; break;
	case 'S': similarity = 1; break;
	case 'h': usage (prog);
	}
    }

  M = data ? fm_read_data_matrix (&dim, stdin)
    : fm_read_distance_matrix (&dim, stdin);

  if (similarity)
    {
      /* convert to distance matrix */
      int size = dim*(dim-1)/2, i;
      for (i = 0; i < size; ++i)
	{
	  if (M[i] < 0. || M[i] > 1.)
	    error ("invalid similarity value: %f", M[i]);
	  else
	    M[i] = sqrt (2.*(1. - M[i]));
	}
    }

  fprintf (stderr, "FastMap: dimension k = %d; "
	   "matrix %dx%d loaded...\n", k, dim, dim);

#if 0
  {
    int i, j;
    
    for (i = 0; i < dim; ++i)
      {
	printf ("%3.0f", valof (M, dim, i, 0));
	for (j = 1; j < dim; ++j)
	  printf (" %3.0f", valof (M, dim, i, j));
	printf ("\n");
      }
  }
#endif

  {
    int i, j;
    double *X = calloc (dim*k,sizeof (double));
    int *P = malloc (2*k*sizeof (int));

    fm_fastmap (X, P, k, M, dim);
    fprintf (output, "%d %d\n", dim, k);
    for (i = 0; i < dim; ++i)
      {
	fprintf (output, "%f", X[i*k]);
	for (j = 1; j < k; ++j)
	  fprintf (output, ",%f", X[i*k+j]);
	fprintf (output, "\n");
      }
#if 0
    fprintf (output, "pivots:\n");
    for (i = 0; i < k; ++i)
      {
	fprintf (output, "%d %d\n", P[i], P[i+k]);
      }
#endif

    free (X);
    free (P);
  }

  if (output != stdout)
    fclose (output);

  free (M);

  return 0;
}


/*
Local Variables:
compile-command: "gcc -Wall -g -o fastmap fastmap.c -lm"
End:
*/
