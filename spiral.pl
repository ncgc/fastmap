#!/usr/bin/perl -w

use strict;

my $N = 30;

my ($x1,$x2,$x3);
my $sqr2 = sqrt(2);
print "$N 3\n";
for my $i (0..$N-1) {
  $x3 = $i/$sqr2;
  $x1 = cos($x3);
  $x2 = sin($x3);
  print "$x1,$x2,$x3\n";
}
