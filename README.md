FastMap Dimensionality Reduction
================================

This is an implementation of the FastMap dimensionality reduction
algorithm; see [here](http://tripod.nih.gov/?p=78) for an example
of 2D-embedding of molecular graphs. To compile, simply type

```
gcc -Wall -g -o fastmap fastmap.c -lm
```

The binary `fastmap` takes as input either a data or
distance/similarity matrix; see `fastmap -h` for additional
details. The Perl script `spiral.pl` provides a simple 2D-embedding 
example of a [3D-spherical spiral](http://en.wikipedia.org/wiki/Spiral)
function:

```
perl spiral.pl | ./fastmap -M 
```


Disclaimer
==========
                         PUBLIC DOMAIN NOTICE
                     NIH Chemical Genomics Center
         National Center for Advancing Translational Sciences

This software/database is a "United States Government Work" under the
terms of the United States Copyright Act.  It was written as part of
the author's official duties as United States Government employee and
thus cannot be copyrighted.  This software is freely available to the
public for use. The NIH Chemical Genomics Center (NCGC) and the
U.S. Government have not placed any restriction on its use or
reproduction.

Although all reasonable efforts have been taken to ensure the accuracy
and reliability of the software and data, the NCGC and the U.S.
Government do not and cannot warrant the performance or results that
may be obtained by using this software or data. The NCGC and the U.S.
Government disclaim all warranties, express or implied, including
warranties of performance, merchantability or fitness for any
particular purpose.

Please cite the authors in any work or product based on this material.
