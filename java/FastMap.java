// $Id: FastMap.java 3495 2009-10-29 15:58:01Z nguyenda $

import java.io.Serializable;
import java.util.Random;
import java.util.Collection;
import java.util.Vector;
import java.util.List;
import java.util.Iterator;
import java.security.SecureRandom;

/**
 * Perform dimensionality reduction using the FastMap embedding
 * algorithm.
 */
public class FastMap<T> {
    private double []coords; // N x dim embedded coordinates

    private int []pivots; // 2 x dim pivots
    // only save the objects that correspond to the pivots....
    private Object[] pivotObjs;
    private double[] pivotDist;

    private int dim; // embeded dimension

    /**
     * Either the entire metric space is specified or
     * both metric and data be specified
     */
    private DataSeq<T> data;
    private Metric<T> metric;
    private MetricSpace space;

    //private static Random rand = new Random (0xdeadbeefl);

    static class DefaultMetricSpace<T> implements MetricSpace {
	// storing the upper triangle of the distance matrix
	double[] matrix; 
	int size;

	DefaultMetricSpace (DataSeq<T> data, Metric<T> metric) {
	    size = data.size();
	    matrix = new double[size*(size-1)/2];
	    int k = 0;
	    for (int i = 0; i < size; ++i) {
		for (int j = i+1; j < size; ++j) {
		    matrix[k++] = metric.compute(data.get(i), data.get(j));
		}
	    }
	}

	int stride (int k) { return k*size - k*(k+1)/2; }
	int index (int i, int j) { 
	    if (i > j) return stride (j) + (i-j-1);
	    if (i < j) return stride (i) + (j-i-1);
	    throw new IllegalArgumentException 
		("No index defined for row "+i + " col " + j);
	}

	public int size () { return size; }
	public double getValue (int i, int j) {
	    if (i == j) return 0.;
	    return matrix[index (i, j)];
	}
    }


    public FastMap (DataSeq<T> data, Metric<T> metric) {
	this.data = data;
	this.metric = metric;
	this.space = new DefaultMetricSpace<T>(data, metric);
    }


    public DataSeq<T> getData () { return data; }
    public Metric<T> getMetric () { return metric; }

    public int getDim () { return dim; }

    /**
     * Return the embedded coordinates of the given data and metric.
     * The returned array is of size data.size() * dim.  The coordinates
     * for the ith member of data starts at position i*dim in the
     * array and spans the next dim elements.
     */
    public double[] embed (int dim) {
	if (metric == null) {
	    throw new IllegalStateException ("No distance metric specified!");
	}
	if (data.size() == 0) {
	    throw new IllegalStateException 
		("No data available to perform embedding!");
	}

	this.dim = dim;
	pivots = new int[2*dim];
	pivotDist = new double[dim];

	coords = embed (dim, space, pivots, pivotDist);

	pivotObjs = new Object[pivots.length];
	for (int i = 0; i < pivots.length; ++i) {
	    pivotObjs[i] = data.get(pivots[i]);
	}

	return coords;
    }

    // perform one-time (no query) embedding of the data
    public static double[] embed (int dim, MetricSpace space) {
	return FastMap.embed(dim, space, null, null);
    }

    public static <T> double[] embed 
	(int dim, DataSeq<T> data, Metric<T> metric) {
	return new FastMap<T> (data, metric).embed(dim);
    }

    private static double[] embed (int dim, MetricSpace M, 
				   int[] pivots, double[] pivotDistance) {

	int size = M.size();
	double[] coords = new double[size*dim];

	int pv[] = new int[2];
	for (int j = 0; j < dim; ++j) {
	    findMaximalPivots (pv, 5, j, dim, coords, M);

	    double ab = projectDistance (pv[0], pv[1], j, dim, coords, M);
	    if (ab > 0.) {
		double ab2 = ab*2., abab = ab*ab, ai, bi;
		for (int i = 0; i < size; ++i) {
		    ai = projectDistance (i, pv[0], j, dim, coords, M);
		    bi = projectDistance (i, pv[1], j, dim, coords, M);
		    coords[i*dim+j] = (double)(ai*ai + abab - bi*bi)/ab2;
		}
	    }
	    else {
		for (int i = 0; i < size; ++i) {
		    coords[i*dim + j] = 0.;
		}
	    }

	    if (pivots != null) {
		pivots[j] = pv[0];
		pivots[j+dim] = pv[1];
	    }

	    if (pivotDistance != null) {
		pivotDistance[j] = ab;
	    }
	}

	return coords;
    }

    // project a query object onto the underlying embedding 
    public double[] project (T query, double[] embed) {
	if (pivotObjs == null || metric == null) {
	    throw new IllegalStateException 
		("Invalid query mode; no data available for projection!");
	}

	if (embed.length != dim) {
	    throw new IllegalArgumentException 
		("Invalid input coordinate dimension!");
	}

	for (int j = 0; j < dim; ++j) {
	    int pa = pivots[j], pb = pivots[j+dim];

	    T objA = (T)pivotObjs[j], objB = (T)pivotObjs[j+dim];
	    double ab = pivotDist[j];

	    if (ab > 0.) {
		double ab2 = ab*2., abab = ab*ab, da, db;

		// project the query on the pivots
		da = metric.compute(objA, query);
		for (int i = 0; i < j; ++i) {
		    double x = embed[i] - coords[pa*dim+i];
		    da = Math.sqrt(Math.abs(da*da - x*x));
		}
		db = metric.compute(objB, query);
		for (int i = 0; i < j; ++i) {
		    double x = embed[i] - coords[pb*dim+i];
		    db = Math.sqrt(Math.abs(db*db - x*x));
		}
		embed[j] = (da*da + abab - db*db)/ab2;
	    }
	    else {
		embed[j] = 0.;
	    }
	}
	return embed;
    }

    public double[] project (T query) {
	return project (query, new double[dim]);
    }

    private static double findMaximalPivots 
	(int[] pv, int niters, int h, int k, double []X, MetricSpace M) {
	double max = Double.NEGATIVE_INFINITY;
	SecureRandom rand = new SecureRandom ();

	int size = M.size();
	pv[0] = 0;
	pv[1] = rand.nextInt(size);
	for (int i = 0; i < niters; ++i) {
	    for (int j = 0; j < size; ++j)
		if (pv[1] != j)  {
		    double x = projectDistance (pv[1], j, h, k, X, M);
		    if (x > max) {
			max = x;
			pv[0] = j;
		    }
		}
	    for (int j = 0; j < size; ++j)
		if (pv[0] != j) {
		    double x = projectDistance (pv[0], j, h, k, X, M);
		    if (x > max)  {
			max = x;
			pv[1] = j;
		    }
		}
	}
	
	return max;
    }

    private static double projectDistance 
	(int a, int b,  int h, int k,  double []X, MetricSpace M) {
	double d = M.getValue(a, b);
	for (int i = 0; i < h; ++i) {
	    double ab = X[a*k+i] - X[b*k+i];
	    d = Math.sqrt(Math.abs (d*d - ab*ab));
	}
	return d;
    }


    private static double euc (double[] x, double y[]) {
	double dis = 0.f;
	for (int i = 0; i < x.length; ++i) {
	    double d = x[i] - y[i];
	    dis += d*d;
	}
	return Math.sqrt(dis);
    }

    static class DataSource implements DataSeq<double[]> {
	Vector<double[]> vec = new Vector<double[]>();
	
	public void add (double[] x) {
	    vec.add(x);
	}
	public int size () { return vec.size(); }
	public double[] get (int index) { return vec.get(index); }
    }

    public static void main (String argv[]) throws Exception {

	Metric<double[]> euclidean = new Metric<double[]>() {
	    public double compute 
	    (double[] x, double[] y) {
		return euc (x, y);
	    }
	};

	DataSource data = new DataSource ();

	double sqr2 = Math.sqrt(2);
	//System.out.println("30 3");
	for (int i = 0; i < 100; ++i) {
	    double[] x = new double[3];
	    x[2] = (double)i/sqr2;
	    x[0] = Math.cos(x[2]);
	    x[1] = Math.sin(x[2]);
	    data.add(x);
	    //System.out.println(x[0]+","+x[1]+","+x[2]);
	}

	FastMap<double[]> fm = new FastMap<double[]>(data, euclidean);

	int dim = 2;
	double[] coords = fm.embed(dim);
	for (int i = 0; i < data.size(); ++i) {
	    System.out.println(coords[i*dim] + " " + coords[i*dim+1]);
	}

	// project new vectors
	System.out.println("\n");
	for (int i = 101; i < 200; ++i) {
	    double[] x = new double[3];
	    x[2] = (double)i/sqr2;
	    x[0] = Math.cos(x[2]);
	    x[1] = Math.sin(x[2]);

	    coords = fm.project(x);
	    System.out.print(coords[0]);
	    for (int j = 1; j < dim; ++j) {
		System.out.print(" " + coords[j]);
	    }
	    System.out.println();
	}

	// project the same vectors 
	System.out.println("\n");
	for (int i = 0; i < 50; ++i) {
	    double[] x = new double[3];
	    x[2] = (double)i/sqr2;
	    x[0] = Math.cos(x[2]);
	    x[1] = Math.sin(x[2]);

	    coords = fm.project(x);
	    System.out.print(coords[0]);
	    for (int j = 1; j < dim; ++j) {
		System.out.print(" " + coords[j]);
	    }
	    System.out.println();
	}
    }
}
