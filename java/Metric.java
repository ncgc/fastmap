// $Id: Metric.java 3467 2009-10-26 21:55:57Z nguyenda $

/**
 * Generic interface for computing a metric between two objects
 */
public interface Metric<T> {
    public double compute (T a, T b);
}
