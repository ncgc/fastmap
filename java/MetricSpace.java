// $Id: MetricSpace.java 3467 2009-10-26 21:55:57Z nguyenda $

/**
 * A metric space is defined as a square, symmetric matrix of 
 * metric values.
 */
public interface MetricSpace {
    public int size (); // return the size of the metric space
    /**
     * return the metric value of the ith and jth objects in
     * the metric space.  Note that a proper metric should, at minimum,
     * satisfies the following: getValue(i,j) == getValue(j,i)
     */
    public double getValue (int i, int j); 
}
